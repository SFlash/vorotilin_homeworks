import java.util.Scanner;

public class Main {
/**
На вход подается последовательность чисел, оканчивающихся на -1.
Необходимо вывести число, которе присутствует в последовательности минимальное количество раз.
Гарантируется:
Все числа в диапазоне от -100 до 100.
Числа встречаются не более 2 147 483 647-раз каждое.
Сложность алгоритма - O(n)
 ---------------
Реализация
1) Создаем целосиленый массив размерности 201 с нулевыми значениями
2) При вводе потока параллельно увеличиваем элемент с индексом вводимого элемента на 1
3) Проводим поиск минимального значения отлисного от 0 и запоминаем его индекс.
 */

    public static void main(String[] args) {

        System.out.println("Введите последовательность,признак конца (-1)");

        Scanner scanner=new Scanner(System.in);
        int[] enteredElementsCount = new int[201]; //Массив в котором будем изменять колличество введеных элементов
        int a=scanner.nextInt();
        while (a!=-1){
            if (a>=-100 && a<=100){
                enteredElementsCount[a+100]++;
            }
            else{
                System.out.println("Элемент лолжен быть в диапозоне от -100 до 100");
            }
            a=scanner.nextInt();
        }
        int minCount=2_147_483_647;
        int minIndex=-1;
        for(int i=0;i<201;i++){
           //System.out.print((i-100)+"---"+enteredElementsCount[i]+"\n");
           if (enteredElementsCount[i]!=0&&minCount>enteredElementsCount[i]){
               minCount=enteredElementsCount[i];
               minIndex=i;
           }
        }
        if (minIndex!=-1){
                System.out.println("Число - ("+(minIndex-100)+") входит в последовательность минимальное количество раз ("+(minCount)+")");
        }
        else{
            System.out.println("Не было введено ни одного элемента");
        }
    }
}
